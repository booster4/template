applicationName="$1"

BACK=$(PWD)
mkdir -p "var"
cd "var" || return
git clone https://github.com/ClickHouse/ClickHouse.git
cd "ClickHouse/docker/server" || return
docker build --ssh default -t "${applicationName}/yandex/clickhouse" --progress=plain -f "Dockerfile.alpine" .
cd "${BACK}" || return
rm -rf "var/ClickHouse"
