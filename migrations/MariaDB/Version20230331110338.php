<?php

declare(strict_types=1);

namespace DoctrineMigrations\MariaDB;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230331110338 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE files (id INT AUTO_INCREMENT NOT NULL, hash VARCHAR(255) NOT NULL, hash_method VARCHAR(255) NOT NULL, size BIGINT NOT NULL COMMENT \'(DC2Type:file_size)\', status VARCHAR(255) NOT NULL, created_at DATETIME(6) NOT NULL, updated_at DATETIME(6) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE files');
    }
}
