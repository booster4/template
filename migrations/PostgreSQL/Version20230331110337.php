<?php

declare(strict_types=1);

namespace DoctrineMigrations\PostgreSQL;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230331110337 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE file_paths (id UUID NOT NULL, file_id UUID NOT NULL, path TEXT NOT NULL, status VARCHAR(255) NOT NULL, created_at TIMESTAMP(6) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(6) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2139362693CB796C ON file_paths (file_id)');
        $this->addSql('COMMENT ON COLUMN file_paths.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN file_paths.file_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE files (id UUID NOT NULL, hash VARCHAR(255) NOT NULL, hash_method VARCHAR(255) NOT NULL, size BIGINT NOT NULL, status VARCHAR(255) NOT NULL, created_at TIMESTAMP(6) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(6) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6354059D1B862B8 ON files (hash)');
        $this->addSql('COMMENT ON COLUMN files.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN files.size IS \'(DC2Type:file_size)\'');
        $this->addSql('ALTER TABLE file_paths ADD CONSTRAINT FK_2139362693CB796C FOREIGN KEY (file_id) REFERENCES files (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE file_paths DROP CONSTRAINT FK_2139362693CB796C');
        $this->addSql('DROP TABLE file_paths');
        $this->addSql('DROP TABLE files');
    }
}
