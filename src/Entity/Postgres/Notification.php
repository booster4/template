<?php

namespace App\Entity\Postgres;

use App\Enum\Notification\Status;
use App\Repository\NotificationRepository;
use App\Repository\NotificationRepositoryInterface;
use Carbon\Carbon;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Uid\Uuid;

#[Entity(repositoryClass: NotificationRepository::class)]
#[Table(name: 'notifications')]
class Notification
{
    #[Id]
    #[Column(type: UuidType::NAME, unique: true, nullable: false)]
    private Uuid $id;

    #[Column(type: 'text', nullable: false)]
    private string $message;

    #[Column(type: 'string', nullable: false, enumType: Status::class)]
    private Status $status;

    #[Column(type: 'datetimetz', precision: 6, nullable: false)]
    private Carbon $createdAt;

    #[Column(type: 'datetimetz', precision: 6, nullable: true)]
    private ?Carbon $updatedAt;

    public function __construct()
    {
        $this->createdAt = Carbon::now();
        $this->status = Status::WAITING;
        $this->updatedAt = null;
    }


    public function getId(): Uuid
    {
        return $this->id;
    }

    public function setId(Uuid $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    public function setStatus(Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): Carbon
    {
        return $this->createdAt;
    }

    public function setCreatedAt(Carbon $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(Carbon $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
