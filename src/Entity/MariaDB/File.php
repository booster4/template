<?php

namespace App\Entity\MariaDB;

use App\Enum\File\Status;
use App\Repository\Legacy\FileRepository;
use App\ValueObject\FileSize;
use Carbon\Carbon;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity(repositoryClass: FileRepository::class)]
#[Table(name: 'files')]
class File
{
    #[Id]
    #[GeneratedValue]
    #[Column(nullable: false)]
    private int $id;

    #[Column(type: "string", nullable: false)]
    private string $hash;

    #[Column(type: "string", nullable: false)]
    private string $hashMethod;

    #[Column(type: "file_size", nullable: false)]
    private FileSize $size;

    #[Column(type: "text", nullable: false)]
    private string $path;

    #[Column(type: "string", nullable: false, enumType: Status::class)]
    private Status $status;

    #[Column(type: "datetimetz", precision: 6, nullable: false)]
    private Carbon $createdAt;

    #[Column(type: "datetimetz", precision: 6, nullable: true)]
    private ?Carbon $updatedAt;

    public function __construct()
    {
        $this->status = Status::ACTIVE;
        $this->createdAt = Carbon::now();
        $this->updatedAt = null;
    }

    public function getID(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function setHash(string $hash): self
    {
        $this->hash = $hash;

        return $this;
    }

    public function getHashMethod(): string
    {
        return $this->hashMethod;
    }

    public function setHashMethod(string $hashMethod): self
    {
        $this->hashMethod = $hashMethod;

        return $this;
    }

    public function getSize(): FileSize
    {
        return $this->size;
    }

    public function setSize(FileSize $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    public function setStatus(Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): Carbon
    {
        return $this->createdAt;
    }

    public function setCreatedAt(Carbon $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?Carbon $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
