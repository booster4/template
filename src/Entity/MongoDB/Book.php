<?php

declare(strict_types=1);

namespace App\Entity\MongoDB;

use Carbon\Carbon;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;

#[Document]
class Book
{
    #[Id]
    private string $id;

    #[Field(type: 'string', nullable: true)]
    private string $title;

    #[Field(type: 'string', nullable: true)]
    private string $author;

    #[Field(type: 'date_immutable', nullable: false)]
    private Carbon $createdAt;

    public function __construct()
    {
        $this->createdAt = Carbon::now();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCreatedAt(): Carbon
    {
        return $this->createdAt;
    }

    public function setCreatedAt(Carbon $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }
}
