<?php

namespace App\Entity\Elasticsearch;

use ElasticsearchBundle\IndexedDocument;

#[IndexedDocument(indexName: 'qwe')]
class File
{
    private string $id;

    private string $hash;

    private array $paths;

    public function __construct()
    {
        $this->paths = [];
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function setHash(string $hash): self
    {
        $this->hash = $hash;

        return $this;
    }

    public function getPaths(): array
    {
        return $this->paths;
    }

    public function setPaths(array $paths): self
    {
        $this->paths = $paths;

        return $this;
    }
}
