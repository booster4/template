<?php

namespace App\Mapper;

use App\Entity\MariaDB\File as LegacyFile;
use App\Entity\Postgres\File;
use App\Enum\File\HashMethod;
use Symfony\Component\Uid\Uuid;

class LegacyFileFileMapper implements LegacyFileFileMapperInterface
{
    public function mapper(LegacyFile $legacyFile, Uuid $fileID): File
    {
        return (new File())
            ->setID($fileID)
            ->setSize($legacyFile->getSize())
            ->setHash($legacyFile->getHash())
            ->setHashMethod($this->hashMethodMapper($legacyFile->getHashMethod()));
    }

    private function hashMethodMapper(string $hashMethod): HashMethod
    {
        return HashMethod::from("file_body_$hashMethod");
    }
}
