<?php

namespace App\Mapper;

use App\Entity\MariaDB\File as LegacyFile;
use App\Entity\Postgres\File\Path;
use Symfony\Component\Uid\Uuid;

interface LegacyFilePathMapperInterface
{
    public function mapper(LegacyFile $file, Uuid $id): Path;
}
