<?php

namespace App\Mapper;

use App\Entity\MariaDB\File as LegacyFile;
use App\Entity\Postgres\File;
use Symfony\Component\Uid\Uuid;

interface LegacyFileFileMapperInterface
{
    public function mapper(LegacyFile $legacyFile, Uuid $fileID): File;
}
