<?php

declare(strict_types=1);

namespace App\Factory\UuidFactory;

use Symfony\Component\Uid\Uuid;

interface UuidFactoryInterface
{
    public function buildV5(string|int|Uuid $seed): Uuid;

    public function buildV7(): Uuid;
}
