<?php

namespace App\Repository\Legacy;

use App\Entity\MariaDB\File;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

class FileRepository extends ServiceEntityRepository implements FileRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, File::class);
    }

    public function getByID(int $id): File
    {
        $file = $this->find($id);

        if (null === $file) {
            throw new Exception('File not found');
        }

        return $file;
    }

    public function getStartingFromID(int $startID, int $limit): array
    {
        return $this
            ->createQueryBuilder('f')
            ->where('f.id > :startID')
            ->setParameter('startID', $startID)
            ->setMaxResults($limit)
            ->getQuery()
            ->execute();
    }
}
