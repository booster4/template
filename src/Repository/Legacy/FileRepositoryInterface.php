<?php

namespace App\Repository\Legacy;

use App\Entity\MariaDB\File;
use Exception;

interface FileRepositoryInterface
{
    /**
     * @throws Exception
     */
    public function getByID(int $id): File;

    /**
     * @return array<File>
     */
    public function getStartingFromID(int $startID, int $limit): array;
}
