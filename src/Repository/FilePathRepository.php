<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Postgres\File;
use App\Entity\Postgres\File\Path;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Persistence\ManagerRegistry;

class FilePathRepository extends ServiceEntityRepository implements FilePathRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Path::class);
    }

    public function getByFile(File $file): Collection
    {
        $paths = $this->findBy(['file' => $file]);

        return new ArrayCollection($paths);
    }
}
