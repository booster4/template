<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Postgres\File;
use App\Enum\File\HashMethod;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Component\Uid\Uuid;

class FileRepository extends ServiceEntityRepository implements FileRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, File::class);
    }

    /**
     * @throws Exception
     */
    public function getByID(Uuid $id): File
    {
        $file = $this->find($id);

        if (null === $file) {
            throw new Exception('File not found');
        }

        return $file;
    }

    /**
     * @throws Exception
     */
    public function getByHash(string $hash, HashMethod $hashMethod): File
    {
        $file = $this->findOneBy([
            'hash' => $hash,
            'hashMethod' => $hashMethod,
        ]);

        if (null === $file) {
            throw new Exception('File not found');
        }

        return $file;
    }
}
