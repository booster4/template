<?php

namespace App\Repository;

use App\Entity\Postgres\File;
use App\Entity\Postgres\Notification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Component\Uid\AbstractUid;

class NotificationRepository extends ServiceEntityRepository implements NotificationRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Notification::class);
    }
    public function getByID(AbstractUid $uuid): Notification
    {
        $notification = $this->find($uuid);

        if (null === $notification) {
            throw new Exception('Notification not found');
        }

        return $notification;
    }
}
