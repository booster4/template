<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Postgres\File;
use App\Enum\File\HashMethod;
use Symfony\Component\Uid\Uuid;

interface FileRepositoryInterface
{
    public function getByID(Uuid $id): File;

    public function getByHash(string $hash, HashMethod $hashMethod): File;
}
