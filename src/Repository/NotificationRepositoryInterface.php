<?php

namespace App\Repository;

use App\Entity\Postgres\Notification;
use Exception;
use Symfony\Component\Uid\AbstractUid;

interface NotificationRepositoryInterface
{
    /**
     * @param AbstractUid $uuid
     *
     * @return Notification
     * @throws Exception
     */
    public function getByID(AbstractUid $uuid): Notification;
}
