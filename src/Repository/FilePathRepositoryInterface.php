<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Postgres\File;
use Doctrine\Common\Collections\Collection;

interface FilePathRepositoryInterface
{
    public function getByFile(File $file): Collection;
}
