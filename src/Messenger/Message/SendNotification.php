<?php

declare(strict_types=1);

namespace App\Messenger\Message;

use Symfony\Component\Uid\AbstractUid;

class SendNotification
{
    public function __construct(
        private readonly AbstractUid $uuid,
    ) {
    }

    public function getID(): AbstractUid
    {
        return $this->uuid;
    }
}
