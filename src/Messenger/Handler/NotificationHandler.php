<?php

namespace App\Messenger\Handler;

use App\Messenger\Message\SendNotification;
use App\Service\NotificationServiceInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class NotificationHandler
{
    public function __construct(private readonly NotificationServiceInterface $notificationService)
    {
    }

    public function __invoke(SendNotification $notification): void
    {
        $this->notificationService->send($notification->getID());
    }
}
