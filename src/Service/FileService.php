<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Postgres\File;
use App\Entity\Postgres\File\Path;
use App\Entity\Elasticsearch\File as SearchFile;
use App\Enum\File\Path\Status as PathStatus;
use App\Enum\File\Status;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Exception;

final class FileService implements FileServiceInterface
{
    private readonly ObjectManager $entityManager;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->entityManager = $managerRegistry->getManager('postgres');
    }

    /**
     * @throws Exception
     */
    public function create(File $file, Path ...$paths): void
    {
        try {
            array_walk($paths, $file->addPath(...));

            $this->entityManager->persist($file);
            $this->entityManager->flush();
        } catch (Exception $exception) {
            throw new Exception('Not created', 0, $exception);
        }
    }

    /**
     * @throws Exception
     */
    public function setActivePath(File $file, Path $activePath): void
    {
        foreach ($file->getPaths() as $path) {
            $path->setStatus(PathStatus::OUTDATED);
        }
        $file->addPath($activePath);
        $activePath->setStatus(PathStatus::ACTIVE);

        $this->entityManager->flush();
    }

    public function delete(File $file): void
    {
        $file->setStatus(Status::DELETE);

        $this->entityManager->flush();
    }

    private function provideFile(File $file): SearchFile
    {
        return (new SearchFile())
            ->setId($file->getID()->toRfc4122())
            ->setHash($file->getHash())
            ->setPaths($file->getPaths()->map(fn (Path $path) => $path->getPath())->toArray());
    }
}
