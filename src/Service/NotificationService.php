<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Postgres\Notification;
use App\Enum\Notification\Status;
use App\Factory\UuidFactory\UuidFactoryInterface;
use App\Messenger\Message\SendNotification;
use App\Repository\NotificationRepositoryInterface;
use Carbon\Carbon;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Uid\AbstractUid;

/**
 * @psalm-suppress PossiblyNullPropertyAssignmentValue
 */
class NotificationService implements NotificationServiceInterface
{
    private readonly ObjectManager $entityManager;

    public function __construct(
        private readonly NotificationRepositoryInterface $notificationRepository,
        private readonly UuidFactoryInterface $uuidFactory,
        private readonly MessageBusInterface $messageBus,
        private readonly LoggerInterface $logger,
        ManagerRegistry $registry,
    ) {
        $this->entityManager = $registry->getManagerForClass(Notification::class);
    }

    public function createAndEnqueue(string $message): void
    {
        $uuid = $this->uuidFactory->buildV7();
        $notification = (new Notification())
            ->setId($uuid)
            ->setMessage($message);

        $this->entityManager->persist($notification);
        $this->entityManager->flush();

        $notification = new SendNotification($uuid);
        $this->messageBus->dispatch($notification);

        $this->logger->info(sprintf('Create and enqueue message "%s"', $message));
    }

    public function send(AbstractUid $uuid): void
    {
        try {
            $notification = $this->notificationRepository->getByID($uuid);

            $notification
                ->setStatus(Status::SENT)
                ->setUpdatedAt(Carbon::now());

            $this->entityManager->flush();
        } catch (Exception) {
            $this->logger->error('Notification not found');
        }
    }
}
