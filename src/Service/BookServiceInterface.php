<?php

namespace App\Service;

use App\Entity\MongoDB\Book;

interface BookServiceInterface
{
    public function create(string $title, string $author): Book;
}
