<?php

namespace App\Service;

use App\Entity\MariaDB\File;

interface LegacyFileMigratorServiceInterface
{
    public function migrate(File $legacyFile): void;

    public function findAllAndMigrate(int $startID, int $batchSize): void;
}
