<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Postgres\File;
use App\Entity\Postgres\File\Path;

interface FileServiceInterface
{
    public function create(File $file, Path ...$paths): void;

    public function setActivePath(File $file, Path $activePath): void;

    public function delete(File $file): void;
}
