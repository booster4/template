<?php

namespace App\Service;

use App\Entity\MariaDB\File as LegacyFile;
use App\Factory\UuidFactory\UuidFactoryInterface;
use App\Mapper\LegacyFileFileMapperInterface;
use App\Mapper\LegacyFilePathMapperInterface;
use App\Repository\Legacy\FileRepositoryInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Generator;

class LegacyFileMigratorService implements LegacyFileMigratorServiceInterface
{
    public function __construct(
        private readonly FileRepositoryInterface $fileRepository,
        private readonly LegacyFileFileMapperInterface $legacyFileFileMapper,
        private readonly LegacyFilePathMapperInterface $legacyFilePathMapper,
        private readonly UuidFactoryInterface $uuidFactory,
        private readonly FileServiceInterface $fileService,
    ) {
    }

    public function migrate(LegacyFile $legacyFile): void
    {
        $fileID = $this->uuidFactory->buildV5($legacyFile->getID());
        $pathID = $this->uuidFactory->buildV5($legacyFile->getPath());

        $file = $this->legacyFileFileMapper->mapper($legacyFile, $fileID);
        $path = $this->legacyFilePathMapper->mapper($legacyFile, $pathID);

        $this->fileService->create($file, $path);
    }

    public function findAllAndMigrate(int $startID, int $batchSize): void
    {
        foreach ($this->getNotMigratedLegacyFiles($startID, $batchSize) as $legacyFile) {
            $this->migrate($legacyFile);
        }
    }

    /**
     * @return Generator<LegacyFile>
     */
    private function getNotMigratedLegacyFiles(int $startID, int $batchSize): Generator
    {
        $id = $startID;
        while (true) {
            $files = $this->fileRepository->getStartingFromID($id, $batchSize);

            if (0 === count($files)) {
                break;
            }
            $id = $files[array_key_last($files)]->getID();

            yield from $files;
        }
    }
}
