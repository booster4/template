<?php

namespace App\Service;

use App\Entity\MongoDB\Book;
use Doctrine\ODM\MongoDB\DocumentManager;

class BookService implements BookServiceInterface
{
    public function __construct(
        private readonly DocumentManager $documentManager,
    ) {
    }

    public function create(string $title, string $author): Book
    {
        $book = (new Book())
            ->setTitle($title)
            ->setAuthor($author);

        $this->documentManager->persist($book);
        $this->documentManager->flush();

        return $book;
    }
}
