<?php

namespace App\Service;

use Symfony\Component\Uid\AbstractUid;

interface NotificationServiceInterface
{
    public function createAndEnqueue(string $message): void;

    public function send(AbstractUid $uuid): void;
}
