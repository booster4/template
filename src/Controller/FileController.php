<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route("/{action}")]
class FileController extends AbstractController
{
    public function __invoke(Request $dto): JsonResponse
    {
        return $this->json($dto);
    }
}
