<?php

namespace App\Controller;

use DateTimeInterface;
use RequestMapperBundle\DTO\MappableRequestInterface;
use Symfony\Component\Serializer\Annotation\Groups;

class Request implements MappableRequestInterface
{
    #[Groups('json')]
    private int $id;

    #[Groups('json')]
    private string $name;

    #[Groups('path')]
    private string $action;

    #[Groups('get')]
    private string $source;

    #[Groups('json')]
    private DateTimeInterface $createdAt;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public function setAction(string $action): self
    {
        $this->action = $action;

        return $this;
    }

    public function getSource(): string
    {
        return $this->source;
    }

    public function setSource(string $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): Request
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
