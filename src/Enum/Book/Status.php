<?php

declare(strict_types=1);

namespace App\Enum\Book;

enum Status: string
{
    case ACTIVE = 'active';

    case DELETE = 'delete';
}
