<?php

declare(strict_types=1);

namespace App\Enum\File;

enum Action: string
{
    case CREATE = 'create';
    case UPDATE = 'update';
    case DELETE = 'delete';
}
