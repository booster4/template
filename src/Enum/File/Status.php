<?php

declare(strict_types=1);

namespace App\Enum\File;

enum Status: string
{
    case ACTIVE = 'active';
    case MISSED = 'missed';
    case DELETE = 'delete';
    case BROKEN = 'broken';
}
