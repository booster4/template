<?php

declare(strict_types=1);

namespace App\Enum\File;

enum HashMethod: string
{
    case FILE_BODY_MD5 = 'file_body_md5';
    case FILE_BODY_SHA = 'file_body_sha';
}
