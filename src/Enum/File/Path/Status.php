<?php

namespace App\Enum\File\Path;

enum Status: string
{
    case ACTIVE = 'active';
    case OUTDATED = 'outdated';
}
