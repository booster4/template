<?php

namespace App\Enum\Notification;

enum Status: string
{
    case WAITING = 'waiting';
    case FAILED = 'failed';
    case OUTDATED = 'outdated';
    case SENT = 'sent';
}
