<?php

declare(strict_types=1);

namespace App\Doctrine\EventListener;

use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Schema\PostgreSQLSchemaManager;
use Doctrine\DBAL\Schema\SchemaException;
use Doctrine\ORM\Tools\Event\GenerateSchemaEventArgs;

/**
 * Doctrine PostgreSQL Default Schema Fix For Symfony
 *
 * @link https://gist.github.com/mvmaasakkers/7c28355715850d991fb9feb649e60463
 * @codeCoverageIgnore
 */
final class FixPostgreSQLDefaultSchemaListener
{
    /**
     * @throws Exception
     * @throws SchemaException
     */
    public function postGenerateSchema(GenerateSchemaEventArgs $args): void
    {
        /** @var PostgreSQLSchemaManager $schemaManager */
        $schemaManager = $args
            ->getEntityManager()
            ->getConnection()
            ->createSchemaManager();

        $schema = $args->getSchema();

        if (method_exists($schemaManager, 'getExistingSchemaSearchPaths')) {
            foreach ($schemaManager->getExistingSchemaSearchPaths() as $namespace) {
                if (!$schema->hasNamespace($namespace)) {
                    $schema->createNamespace($namespace);
                }
            }
        }
    }
}
