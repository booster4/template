<?php

namespace App\Command;

use App\Service\NotificationServiceInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand('notifications:send', 'Send custom notification')]
class SendNotificationCommand extends Command
{
    public function __construct(
        private readonly NotificationServiceInterface $notificationService,
    ) {
        parent::__construct();
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->notificationService->createAndEnqueue($input->getArgument('message'));

        return self::SUCCESS;
    }

    protected function configure(): void
    {
        $this->addArgument('message', InputArgument::REQUIRED, 'Message');
    }
}
