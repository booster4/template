<?php

namespace App\Command;

use ElasticsearchBundle\Connection;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'test:command')]
class TestCommand extends Command
{

    public function __construct(private readonly Connection $connection)
    {
        parent::__construct();
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        dump($this->connection->info()['version']['number']);

        return self::SUCCESS;
    }
}
