<?php

declare(strict_types=1);

namespace App\Tests\Integration\Service;

use App\Entity\Postgres\Notification;
use App\Enum\Notification\Status;
use App\Factory\UuidFactory\UuidFactoryInterface;
use App\Messenger\Message\SendNotification;
use App\Repository\NotificationRepositoryInterface;
use App\Service\NotificationService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Uid\Uuid;

class NotificationServiceTest extends TestCase
{
    public function testSuccessSend(): void
    {
        $uuid = Uuid::v4();
        $message = 'Some message';
        $notification = (new Notification())
            ->setId($uuid)
            ->setMessage($message);

        $notificationRepository = $this->createMock(NotificationRepositoryInterface::class);
        $uuidFactory = $this->createMock(UuidFactoryInterface::class);
        $messageBus = $this->createMock(MessageBusInterface::class);
        $entityManage = $this->createMock(EntityManagerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $registry = $this->createMock(ManagerRegistry::class);

        $registry
            ->expects($this->once())
            ->method('getManagerForClass')
            ->with(Notification::class)
            ->willReturn($entityManage);

        $notificationRepository
            ->expects($this->once())
            ->method('getByID')
            ->with($uuid)
            ->willReturn($notification);

        $service = new NotificationService($notificationRepository, $uuidFactory, $messageBus, $logger, $registry);
        $service->send($uuid);

        $this->assertEquals(Status::SENT, $notification->getStatus());
        $this->assertNotNull($notification->getUpdatedAt());
    }

    public function testFailSend(): void
    {
        $uuid = Uuid::v4();

        $notificationRepository = $this->createMock(NotificationRepositoryInterface::class);
        $uuidFactory = $this->createMock(UuidFactoryInterface::class);
        $messageBus = $this->createMock(MessageBusInterface::class);
        $entityManage = $this->createMock(EntityManagerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $registry = $this->createMock(ManagerRegistry::class);

        $registry
            ->expects($this->once())
            ->method('getManagerForClass')
            ->with(Notification::class)
            ->willReturn($entityManage);

        $notificationRepository
            ->expects($this->once())
            ->method('getByID')
            ->willThrowException(new Exception('Notification not found'));

        $logger
            ->expects($this->once())
            ->method('error');

        $service = new NotificationService($notificationRepository, $uuidFactory, $messageBus, $logger, $registry);
        $service->send($uuid);
    }

    public function testCreateAndEnqueue(): void
    {
        $uuid = Uuid::v4();
        $expectedMessage = new SendNotification($uuid);

        $notificationRepository = $this->createMock(NotificationRepositoryInterface::class);
        $uuidFactory = $this->createMock(UuidFactoryInterface::class);
        $messageBus = $this->createMock(MessageBusInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $registry = $this->createMock(ManagerRegistry::class);

        $uuidFactory
            ->expects($this->once())
            ->method('buildV7')
            ->willReturn($uuid);

        $messageBus
            ->expects($this->once())
            ->method('dispatch')
            ->with(self::callback(function (SendNotification $message) use ($expectedMessage) {
                $this->assertEquals($message, $expectedMessage);

                return true;
            }))
            ->willReturn(new Envelope($expectedMessage, []));

        $entityManager
            ->expects($this->once())
            ->method('persist');

        $entityManager
            ->expects($this->once())
            ->method('flush');

        $registry
            ->expects($this->once())
            ->method('getManagerForClass')
            ->with(Notification::class)
            ->willReturn($entityManager);

        $service = new NotificationService($notificationRepository, $uuidFactory, $messageBus, $logger, $registry);

        $service->createAndEnqueue('Some message');
    }
}
