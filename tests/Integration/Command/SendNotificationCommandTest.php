<?php

namespace App\Tests\Integration\Command;

use App\Command\SendNotificationCommand;
use App\Service\NotificationServiceInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Tester\CommandTester;

class SendNotificationCommandTest extends TestCase
{
    public function testExecute(): void
    {
        $message = 'Custom message';

        $service = $this->createMock(NotificationServiceInterface::class);
        $service
            ->expects($this->once())
            ->method('createAndEnqueue')
            ->with($message);

        $command = new SendNotificationCommand($service);

        $commandTester = new CommandTester($command);

        $commandTester->execute(['message' => $message]);
    }
}
