<?php

namespace App\Tests\Integration\Messenger\Handler;

use App\Messenger\Handler\NotificationHandler;
use App\Messenger\Message\SendNotification;
use App\Service\NotificationServiceInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\Uuid;

class NotificationHandlerTest extends TestCase
{
    public function testInvoke(): void
    {
        $uuid = Uuid::v4();
        $sendNotification = new SendNotification($uuid);

        $notificationService = $this->createMock(NotificationServiceInterface::class);
        $notificationService
            ->expects($this->once())
            ->method('send')
            ->with($sendNotification->getID());

        $notificationHandler = new NotificationHandler($notificationService);
        $notificationHandler($sendNotification);
    }
}
