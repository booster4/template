<?php

namespace App\Tests\Unit\Messenger\Message;

use App\Messenger\Message\SendNotification;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\Uuid;

class SendNotificationTest extends TestCase
{
    public function testConstructor(): void
    {
        $uuid = Uuid::v4();

        $sendNotification = new SendNotification($uuid);

        $this->assertEquals($uuid, $sendNotification->getID());
    }
}
