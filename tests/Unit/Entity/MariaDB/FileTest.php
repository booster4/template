<?php

namespace App\Tests\Unit\Entity\MariaDB;

use App\Entity\MariaDB\File;
use App\Enum\File\Status;
use App\ValueObject\FileSize;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class FileTest extends TestCase
{
    public function testConstructor(): void
    {
        $file = new File();

        $this->assertEquals(Status::ACTIVE, $file->getStatus());
        $this->assertNotNull($file->getCreatedAt());
        $this->assertNull($file->getUpdatedAt());
    }

    public function testGettersAndSetters(): void
    {
        $id = 1000;
        $hash = 'fake-hash';
        $hashMethod = 'md5';
        $status = Status::BROKEN;
        $path = '/var/log/fpm.log';
        $createdAt = Carbon::now();
        $updatedAt = Carbon::now();
        $size = FileSize::new(1024);

        $file = (new File())
            ->setId($id)
            ->setHash($hash)
            ->setHashMethod($hashMethod)
            ->setStatus($status)
            ->setPath($path)
            ->setCreatedAt($createdAt)
            ->setUpdatedAt($updatedAt)
            ->setSize($size);

        $this->assertEquals($id, $file->getID());
        $this->assertEquals($hash, $file->getHash());
        $this->assertEquals($hashMethod, $file->getHashMethod());
        $this->assertEquals($status, $file->getStatus());
        $this->assertEquals($path, $file->getPath());
        $this->assertEquals($createdAt, $file->getCreatedAt());
        $this->assertEquals($updatedAt, $file->getUpdatedAt());
        $this->assertEquals($size, $file->getSize());
    }
}
