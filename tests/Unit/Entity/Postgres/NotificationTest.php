<?php

namespace App\Tests\Unit\Entity\Postgres;

use App\Entity\Postgres\Notification;
use App\Enum\Notification\Status;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\Uuid;

class NotificationTest extends TestCase
{
    public function testConstructor(): void
    {
        $notification = new Notification();

        $this->assertEquals(Status::WAITING, $notification->getStatus());
        $this->assertNull($notification->getUpdatedAt());
    }

    public function testGettersAndSetters(): void
    {
        $createdAt = Carbon::now();
        $updatedAt = Carbon::now();
        $status = Status::OUTDATED;
        $message = 'Some message';
        $id = Uuid::v4();

        $notification = (new Notification())
            ->setId($id)
            ->setStatus($status)
            ->setMessage($message)
            ->setCreatedAt($createdAt)
            ->setUpdatedAt($updatedAt);

        $this->assertEquals($id, $notification->getId());
        $this->assertEquals($status, $notification->getStatus());
        $this->assertEquals($message, $notification->getMessage());
        $this->assertEquals($createdAt, $notification->getCreatedAt());
        $this->assertEquals($updatedAt, $notification->getUpdatedAt());
    }
}
