<?php

declare(strict_types=1);

namespace App\Tests\Unit\Entity\MongoDB;

use App\Entity\MongoDB\Book;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class BookTest extends TestCase
{
    public function testGettersAndSetters(): void
    {
        $id = 'id';
        $title = 'title';
        $author = 'author';
        $createdAt = Carbon::now();

        $book = (new Book())
            ->setId($id)
            ->setTitle($title)
            ->setAuthor($author)
            ->setCreatedAt($createdAt);

        $this->assertEquals($id, $book->getId());
        $this->assertEquals($title, $book->getTitle());
        $this->assertEquals($author, $book->getAuthor());
        $this->assertEquals($createdAt, $book->getCreatedAt());
    }
}
