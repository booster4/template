<?php

declare(strict_types=1);

namespace App\Tests\Unit\DBAL\Type;

use App\DBAL\Type\FileSize;
use App\ValueObject\FileSize as FileSizeVO;
use Doctrine\DBAL\Platforms\PostgreSQLPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Exception;
use PHPUnit\Framework\TestCase;

class FileSizeTest extends TestCase
{
    public function testGetSQLDeclaration(): void
    {
        $fileSize = new FileSize();

        $platform = new PostgreSQLPlatform();
        $stmt = $fileSize->getSQLDeclaration(['file_size'], $platform);

        $this->assertEquals($platform->getBigIntTypeDeclarationSQL([]), $stmt);
    }

    /**
     * @throws ConversionException
     */
    public function testConvertToPHPValueValueObject(): void
    {
        $size = 2048;

        $fileSize = new FileSize();
        $platform = new PostgreSQLPlatform();

        /** @var FileSizeVO $valueObject */
        $valueObject = $fileSize->convertToPHPValue($size, $platform);

        $this->assertInstanceOf(FileSizeVO::class, $valueObject);
        $this->assertEquals($size, $valueObject->getSize());
    }

    /**
     * @throws ConversionException
     */
    public function testConvertToPHPValueNull(): void
    {
        $fileSize = new FileSize();
        $platform = new PostgreSQLPlatform();

        $valueObject = $fileSize->convertToPHPValue(null, $platform);

        $this->assertNull($valueObject);
    }

    /**
     * @throws Exception
     */
    public function testConvertToDatabaseValue(): void
    {
        $size = 2048;
        $fileSize = new FileSize();
        $platform = new PostgreSQLPlatform();

        $value = $fileSize->convertToDatabaseValue((new FileSizeVO($size)), $platform);

        $this->assertEquals((string) $size, $value);
    }

    /**
     * @throws Exception
     */
    public function testConvertToDatabaseValueNull(): void
    {
        $fileSize = new FileSize();
        $platform = new PostgreSQLPlatform();

        $value = $fileSize->convertToDatabaseValue(null, $platform);

        $this->assertNull($value);
    }

    /**
     * @throws ConversionException
     */
    public function testConvertToDatabaseValueException(): void
    {
        $fileSize = new FileSize();
        $platform = new PostgreSQLPlatform();

        $this->expectException(Exception::class);

        $fileSize->convertToDatabaseValue("foo", $platform);
    }

    public function testGetName(): void
    {
        $fileSize = new FileSize();

        $this->assertEquals(FileSize::NAME, $fileSize->getName());
    }
}
