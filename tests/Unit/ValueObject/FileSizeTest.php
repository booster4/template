<?php

declare(strict_types=1);

namespace App\Tests\Unit\ValueObject;

use App\ValueObject\FileSize;
use Exception;
use Generator;
use PHPUnit\Framework\TestCase;

class FileSizeTest extends TestCase
{
    public function testConstructor(): void
    {
        $size = 1000;
        $fileSize = new FileSize($size);

        $this->assertEquals($size, $fileSize->getSize());
        $this->assertTrue($fileSize->isStrict());

        $fileSize = new FileSize($size, false);

        $this->assertFalse($fileSize->isStrict());
    }

    /**
     * @dataProvider toStringDataProvider
     * @throws Exception
     */
    public function testToString(string $expected, int $bytes, bool $strict): void
    {
        $fileSize = new FileSize($bytes, $strict);

        $this->assertEquals($expected, $fileSize->toString());
    }

    public function toStringDataProvider(): Generator
    {
        yield ['expected' => '≈1000B', 'bytes' => 1000, 'strict' => false];
        yield ['expected' => '1.00KB', 'bytes' => 1024, 'strict' => true];
        yield ['expected' => '1.03KB', 'bytes' => 1058, 'strict' => true];
        yield ['expected' => '1B', 'bytes' => 1, 'strict' => true];
        yield ['expected' => '32.46GB', 'bytes' => 34857387453, 'strict' => true];
        yield ['expected' => '20.47PB', 'bytes' => 23045825717874561, 'strict' => true];
        yield ['expected' => '≈3.00TB', 'bytes' => 3298534883328, 'strict' => false];
    }
}
