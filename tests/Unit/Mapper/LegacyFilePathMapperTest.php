<?php

namespace App\Tests\Unit\Mapper;

use App\Entity\MariaDB\File;
use App\Mapper\LegacyFilePathMapper;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\Uuid;

class LegacyFilePathMapperTest extends TestCase
{
    public function testMapper(): void
    {
        $legacyFile = (new File())
            ->setPath('/var/log/some.log');

        $mapper = new LegacyFilePathMapper();
        $uuid = Uuid::v4();

        $path = $mapper->mapper($legacyFile, $uuid);

        $this->assertEquals($uuid, $path->getID());
        $this->assertEquals($legacyFile->getPath(), $path->getPath());
    }
}
