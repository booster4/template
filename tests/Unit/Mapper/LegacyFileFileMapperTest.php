<?php

namespace App\Tests\Unit\Mapper;

use App\Entity\MariaDB\File;
use App\Mapper\LegacyFileFileMapper;
use App\ValueObject\FileSize;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\Uuid;

class LegacyFileFileMapperTest extends TestCase
{
    public function testMapper(): void
    {
        $legacyFile = (new File())
            ->setHash('hash')
            ->setHashMethod('md5')
            ->setSize(FileSize::new(1024))
            ->setPath('/var/log/some.log')
            ->setUpdatedAt(Carbon::now());

        $fileMapper = new LegacyFileFileMapper();

        $fileID = Uuid::v4();

        $file = $fileMapper->mapper($legacyFile, $fileID);

        $this->assertEquals($fileID, $file->getID());
        $this->assertEquals($legacyFile->getHash(), $file->getHash());
        $this->assertEquals("file_body_{$legacyFile->getHashMethod()}", $file->getHashMethod()->value);
        $this->assertEquals($legacyFile->getSize(), $file->getSize());
        $this->assertCount(0, $file->getPaths());
    }
}
