<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

abstract class TransactionalTestCase extends KernelTestCase
{
    protected EntityManagerInterface $entityManager;

    /**
     * @throws Exception
     */
    public function setUp(): void
    {
        $em = self::getContainer()->get(EntityManagerInterface::class);

        if ($em instanceof EntityManagerInterface) {
            $this->entityManager = $em;
            $this->entityManager->beginTransaction();
        }
    }

    public function tearDown(): void
    {
        $this->entityManager->rollback();

        parent::tearDown();
    }
}
