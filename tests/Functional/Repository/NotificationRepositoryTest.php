<?php

namespace App\Tests\Functional\Repository;

use App\Entity\Postgres\Notification;
use App\Enum\Notification\Status;
use App\Repository\NotificationRepository;
use App\Repository\NotificationRepositoryInterface;
use App\Tests\Functional\TransactionalTestCase;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Exception;
use Symfony\Component\Uid\Uuid;

/**
 * @psalm-suppress PossiblyNullReference
 * @psalm-suppress PropertyTypeCoercion
 */
class NotificationRepositoryTest extends TransactionalTestCase
{
    private NotificationRepositoryInterface $notificationRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->notificationRepository = self::getContainer()->get(NotificationRepository::class);
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     * @throws Exception
     */
    public function testSuccessGetByID(): void
    {
        $id = Uuid::v4();
        $expectedNotification = (new Notification())
            ->setID($id)
            ->setStatus(Status::OUTDATED)
            ->setMessage('Some message');

        $this->entityManager->persist($expectedNotification);
        $this->entityManager->flush();

        $actualNotification = $this->notificationRepository->getByID($id);

        $this->assertEquals($expectedNotification, $actualNotification);
    }

    public function testFailGetByID(): void
    {
        $id = Uuid::v4();

        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Notification not found');
        $this->notificationRepository->getByID($id);
    }
}
