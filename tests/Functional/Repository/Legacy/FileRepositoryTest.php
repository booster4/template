<?php

namespace App\Tests\Functional\Repository\Legacy;

use App\Entity\MariaDB\File;
use App\Repository\Legacy\FileRepository;
use App\Tests\Functional\MariaDbTransactionalTestCase;
use App\ValueObject\FileSize;
use Exception;

/**
 * @psalm-suppress PropertyTypeCoercion
 */
class FileRepositoryTest extends MariaDbTransactionalTestCase
{
    private FileRepository $fileRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->fileRepository = self::getContainer()->get(FileRepository::class);
    }

    /**
     * @throws Exception
     */
    public function testSuccessfulGetByID(): void
    {
        $expectedFile = (new File())
            ->setHash(md5('fake-seed'))
            ->setHashMethod('md5')
            ->setPath('/var/log/some.log')
            ->setSize(new FileSize(1024));

        $this->entityManager->persist($expectedFile);
        $this->entityManager->flush();
        $this->entityManager->clear();

        $actualFile = $this->fileRepository->getByID($expectedFile->getID());

        $this->assertEquals($expectedFile, $actualFile);
    }

    public function testFailGetByID(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('File not found');
        $this->fileRepository->getByID(-1);
    }
}
