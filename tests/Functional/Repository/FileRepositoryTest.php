<?php

declare(strict_types=1);

namespace App\Tests\Functional\Repository;

use App\Entity\Postgres\File;
use App\Enum\File\HashMethod;
use App\Repository\FileRepository;
use App\Repository\FileRepositoryInterface;
use App\Tests\Functional\TransactionalTestCase;
use App\ValueObject\FileSize;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Exception;
use Symfony\Component\Uid\Uuid;

/**
 * @psalm-suppress PossiblyNullReference
 * @psalm-suppress PropertyTypeCoercion
 */
class FileRepositoryTest extends TransactionalTestCase
{
    private ?FileRepositoryInterface $fileRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->fileRepository = self::getContainer()->get(FileRepository::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testSuccessGetByID(): void
    {
        $id = Uuid::v4();
        $expectedFile = (new File())
            ->setID($id)
            ->setSize(new FileSize(1024))
            ->setHash('hash')
            ->setHashMethod(HashMethod::FILE_BODY_MD5);

        $this->entityManager->persist($expectedFile);
        $this->entityManager->flush();

        $actualFile = $this->fileRepository->getByID($id);

        $this->assertEquals($expectedFile, $actualFile);
    }

    public function testFailGetByID(): void
    {
        $id = Uuid::v4();

        $this->expectException(Exception::class);
        $this->expectExceptionMessage('File not found');
        $this->fileRepository->getByID($id);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testSuccessGetByHash(): void
    {
        $id = Uuid::v4();
        $hash = hash('md5', (string) rand(0, 100));
        $hashMethod = HashMethod::FILE_BODY_MD5;
        $expectedFile = (new File())
            ->setID($id)
            ->setSize(new FileSize(1024))
            ->setHash($hash)
            ->setHashMethod($hashMethod);

        $this->entityManager->persist($expectedFile);
        $this->entityManager->flush();

        $actualFile = $this->fileRepository->getByHash($hash, $hashMethod);

        $this->assertEquals($expectedFile, $actualFile);
    }

    public function testFailGetByHash(): void
    {
        $hash = 'fake-hash';

        $this->expectException(Exception::class);
        $this->expectExceptionMessage('File not found');
        $this->fileRepository->getByHash($hash, HashMethod::FILE_BODY_MD5);
    }
}
