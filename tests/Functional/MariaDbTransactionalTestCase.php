<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

abstract class MariaDbTransactionalTestCase extends KernelTestCase
{
    protected EntityManagerInterface $entityManager;

    /**
     * @throws Exception
     */
    public function setUp(): void
    {
        /** @var ManagerRegistry $managerRegistry */
        $managerRegistry = self::getContainer()->get(ManagerRegistry::class);

        $em = $managerRegistry->getManager('mariadb');
        if ($em instanceof EntityManagerInterface) {
            $this->entityManager = $em;
            $this->entityManager->beginTransaction();
        }
    }

    public function tearDown(): void
    {
        $this->entityManager->rollback();

        parent::tearDown();
    }
}
