<?php

declare(strict_types=1);

namespace App\Tests\Functional\DBAL\Type;

use App\Entity\Postgres\File;
use App\Repository\FileRepository;
use App\Tests\Functional\TransactionalTestCase;
use App\Enum\File\HashMethod;
use App\ValueObject\FileSize;
use Exception;
use Symfony\Component\Uid\Uuid;

class FileSizeTest extends TransactionalTestCase
{
    /**
     * @throws Exception
     */
    public function testMapping(): void
    {
        $size = 438575673;
        $id = Uuid::v7();

        $file = (new File())
            ->setID($id)
            ->setHash(md5('foo'))
            ->setHashMethod(HashMethod::FILE_BODY_MD5)
            ->setSize(new FileSize($size));

        $this->entityManager->persist($file);
        $this->entityManager->flush();

        /** @var FileRepository $fileRepository */
        $fileRepository = self::getContainer()->get(FileRepository::class);

        $result = $fileRepository->getByID($id);

        $this->assertEquals($size, $result->getSize()->getSize());
    }
}
