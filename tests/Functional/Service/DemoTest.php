<?php

namespace App\Tests\Functional\Service;

use App\Tests\Functional\TransactionalTestCase;
use ElasticsearchBundle\Connection;

class DemoTest extends TransactionalTestCase
{
    private Connection $client;

    public function setUp(): void
    {
        parent::setUp();

        $this->client = self::getContainer()->get(Connection::class);
    }

    public function testRun(): void
    {
        dd($this->client->info());
    }
}
