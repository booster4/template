<?php

declare(strict_types=1);

namespace App\Tests\Functional\Service;

use App\Service\BookService;
use App\Service\BookServiceInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @psalm-suppress PossiblyNullReference
 * @psalm-suppress PossiblyNullArgument
 * @psalm-suppress PropertyTypeCoercion
 * @psalm-suppress PossiblyNullReference
 */
class BookServiceTest extends KernelTestCase
{
    private BookServiceInterface $bookService;

    /**
     * @throws Exception
     */
    public function setUp(): void
    {
        $this->bookService = self::getContainer()->get(BookService::class);
    }

    public function testCreate(): void
    {
        $title = 'Title';
        $author = 'Author';

        $book = $this->bookService->create($title, $author);

        $this->assertEquals($title, $book->getTitle());
        $this->assertEquals($author, $book->getAuthor());
    }
}
