<?php

declare(strict_types=1);

namespace App\Tests\Functional\Service;

use App\Entity\Postgres\File;
use App\Entity\Postgres\File\Path;
use App\Enum\File\HashMethod;
use App\Enum\File\Path\Status as PathStatus;
use App\Enum\File\Status;
use App\Factory\UuidFactory\UuidFactoryInterface;
use App\Repository\FilePathRepositoryInterface;
use App\Repository\FileRepositoryInterface;
use App\Service\FileService;
use App\Service\FileServiceInterface;
use App\Tests\Functional\TransactionalTestCase;
use App\ValueObject\FileSize;
use Exception;
use Symfony\Component\Uid\Uuid;

/**
 * @psalm-suppress PossiblyNullReference
 * @psalm-suppress PossiblyNullArgument
 * @psalm-suppress PropertyTypeCoercion
 * @psalm-suppress PossiblyNullReference
 */
class FileServiceTest extends TransactionalTestCase
{
    private ?FileServiceInterface $fileService;

    private ?FileRepositoryInterface $fileRepository;

    private ?UuidFactoryInterface $uuidFactory;

    private ?FilePathRepositoryInterface $filePathRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->fileService = self::getContainer()->get(FileService::class);
        $this->fileRepository = self::getContainer()->get(FileRepositoryInterface::class);
        $this->uuidFactory = self::getContainer()->get(UuidFactoryInterface::class);
        $this->filePathRepository = self::getContainer()->get(FilePathRepositoryInterface::class);
    }

    public function testCreate(): void
    {
        $expectedPath = (new Path())
            ->setID($this->uuidFactory->buildV5('pathID'))
            ->setPath('/var/path/some.log');

        $expectedFile = (new File())
            ->setID($this->uuidFactory->buildV5('fileID'))
            ->setHashMethod(HashMethod::FILE_BODY_MD5)
            ->setHash(md5((string) rand(0, 100)))
            ->addPath($expectedPath)
            ->setSize(new FileSize(1024));

        $this->fileService->create($expectedFile, $expectedPath);

        $actualFile = $this->fileRepository->getByID($expectedFile->getID());
        $this->assertEquals($expectedFile, $actualFile);

        $paths = $this->filePathRepository->getByFile($expectedFile);
        $this->assertCount(1, $paths);
        $this->assertEquals($expectedPath, $paths->get(0));
    }

    public function testCreateFileWithoutPath(): void
    {
        $expectedFile = (new File())
            ->setHashMethod(HashMethod::FILE_BODY_MD5)
            ->setHash(md5((string) rand(0, 100)))
            ->setSize(new FileSize(1024));

        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Not create');
        $this->expectExceptionCode(0);
        $this->fileService->create($expectedFile);
    }

    public function testSetActivePath(): void
    {
        $file = (new File())
            ->setID(Uuid::v4())
            ->setSize(new FileSize(1024))
            ->setHash('hash')
            ->setHashMethod(HashMethod::FILE_BODY_MD5);

        $path = (new Path())
            ->setID(Uuid::v4())
            ->setPath('/var/log/some.log');

        $file->addPath($path);

        $this->entityManager->persist($file);
        $this->entityManager->flush();

        $activePath = (new Path())
            ->setID(Uuid::v4())
            ->setPath('/var/log/another.log');

        $this->fileService->setActivePath($file, $activePath);

        $this->assertEquals(PathStatus::OUTDATED, $path->getStatus());
        $this->assertEquals(PathStatus::ACTIVE, $activePath->getStatus());

        $this->fileService->setActivePath($file, $path);

        $this->assertEquals(PathStatus::OUTDATED, $activePath->getStatus());
        $this->assertEquals(PathStatus::ACTIVE, $path->getStatus());
    }

    public function testDelete(): void
    {
        $file = (new File())
            ->setID(Uuid::v4())
            ->setHash('hash')
            ->setHashMethod(HashMethod::FILE_BODY_MD5)
            ->setSize(new FileSize(1024));

        $this->fileService->delete($file);

        $this->assertEquals(Status::DELETE, $file->getStatus());
    }
}
