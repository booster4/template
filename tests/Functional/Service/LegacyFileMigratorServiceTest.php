<?php

namespace App\Tests\Functional\Service;

use App\Entity\MariaDB\File;
use App\Enum\File\HashMethod;
use App\Repository\FileRepositoryInterface;
use App\Service\LegacyFileMigratorService;
use App\Tests\Functional\TransactionalTestCase;
use App\ValueObject\FileSize;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;

/**
 * @psalm-suppress PossiblyNullReference
 * @psalm-suppress PossiblyNullArgument
 * @psalm-suppress PropertyTypeCoercion
 */
class LegacyFileMigratorServiceTest extends TransactionalTestCase
{
    private ObjectManager $mariadbObjectManager;

    private LegacyFileMigratorService $legacyFileMigratorService;

    private FileRepositoryInterface $fileRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->mariadbObjectManager = self::getContainer()->get(ManagerRegistry::class)->getManager('mariadb');
        $this->legacyFileMigratorService = self::getContainer()->get(LegacyFileMigratorService::class);
        $this->fileRepository = self::getContainer()->get(FileRepositoryInterface::class);
    }

    public function testFindAllAndMigrate(): void
    {
        $legacyFiles = [];

        $legacyFilesData = [
            ['hash-1', 'md5', 1024, '/var/log/log-1.log'],
            ['hash-2', 'md5', 2048, '/var/log/log-2.log'],
            ['hash-3', 'md5', 4096, '/var/log/log-3.log'],
        ];

        foreach ($legacyFilesData as $value) {
            $file = (new File())
                ->setHash($value[0])
                ->setHashMethod($value[1])
                ->setSize(FileSize::new($value[2]))
                ->setPath($value[3]);

            $this->mariadbObjectManager->persist($file);

            $legacyFiles[] = $file;
        }
        $this->mariadbObjectManager->flush();

        $lastID = $legacyFiles[0]->getID();

        $this->legacyFileMigratorService->findAllAndMigrate($lastID, 100);

        /** @var File $legacyFile */
        foreach (array_slice($legacyFiles, 1) as $legacyFile) {
            $file = $this->fileRepository->getByHash(
                $legacyFile->getHash(),
                HashMethod::from("file_body_{$legacyFile->getHashMethod()}"),
            );

            $this->assertEquals($legacyFile->getSize(), $file->getSize());
        }
    }
}
