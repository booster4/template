<?php

namespace RequestMapperBundle\DataExtractor;

 use Symfony\Component\HttpFoundation\Request;

final class GetParametersExtractor implements DataExtractorInterface
{
    public function extract(Request $request): array
    {
         return $request->query->all();
    }

    public function getGroups(): array
    {
         return ['get'];
    }
}
