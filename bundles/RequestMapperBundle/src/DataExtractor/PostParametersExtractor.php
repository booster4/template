<?php

namespace RequestMapperBundle\DataExtractor;

use Symfony\Component\HttpFoundation\Request;

class PostParametersExtractor implements DataExtractorInterface
{
    public function extract(Request $request): array
    {
        return $request->request->all();
    }

    public function getGroups(): array
    {
        return ['post'];
    }
}
