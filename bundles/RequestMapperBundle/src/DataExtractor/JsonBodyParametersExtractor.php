<?php

namespace RequestMapperBundle\DataExtractor;

use Symfony\Component\HttpFoundation\Request;

class JsonBodyParametersExtractor implements DataExtractorInterface
{
    public function extract(Request $request): array
    {
        return json_decode($request->getContent(), true);
    }

    public function getGroups(): array
    {
        return ['json'];
    }
}
