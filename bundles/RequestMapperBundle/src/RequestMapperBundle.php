<?php

namespace RequestMapperBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class RequestMapperBundle extends Bundle
{
    public function getPath(): string
    {
        return dirname(__DIR__);
    }
}
