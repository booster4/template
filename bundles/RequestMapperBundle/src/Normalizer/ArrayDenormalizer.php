<?php

namespace RequestMapperBundle\Normalizer;

use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class ArrayDenormalizer implements ArrayDenormalizerInterface
{
    public function __construct(private readonly SerializerInterface&DenormalizerInterface $serializer)
    {
    }

    /**
     * @throws ExceptionInterface
     */
    public function denormalize(array $data, string $class, array $groups, object $object): object
    {
        return $this->serializer->denormalize(
            $data,
            $class,
            'array',
            $this->provideDeserializeOptions($groups, $object),
        );
    }

    /**
     * @param string[] $groups
     *
     * @return array<string, mixed>
     */
    private function provideDeserializeOptions(array $groups, object $object): array
    {
        return [
            AbstractNormalizer::GROUPS => $groups,
            AbstractObjectNormalizer::DISABLE_TYPE_ENFORCEMENT => true,
            AbstractNormalizer::OBJECT_TO_POPULATE => $object,
        ];
    }
}
