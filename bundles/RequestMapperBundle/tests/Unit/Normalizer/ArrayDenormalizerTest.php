<?php

namespace RequestMapperBundle\Tests\Unit\Normalizer;

use PHPUnit\Framework\TestCase;
use RequestMapperBundle\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ArrayDenormalizerTest extends TestCase
{
    /**
     * @throws ExceptionInterface
     */
    public function testDenormalize(): void
    {
        $id = 11;
        $name = 'John';
        $surname = 'Doe';

        $get = ['id' => $id];
        $post = ['name' => $name, 'surname' => $surname];

        $obj = $this->provideAnonymousClassObject();

        $serializer = new Serializer([new ObjectNormalizer()]);
        $arrayDenormalizer = new ArrayDenormalizer($serializer);

        $arrayDenormalizer->denormalize($post, $obj::class, ['post'], $obj);
        $arrayDenormalizer->denormalize($get, $obj::class, ['get'], $obj);

        $this->assertEquals($id, $obj->getId());
        $this->assertEquals($name, $obj->getName());
        $this->assertEquals($surname, $obj->getSurname());
    }

    protected function provideAnonymousClassObject(): object
    {
        return new class {
            #[Groups('get')]
            private int $id;
            #[Groups('post')]
            private string $name;
            #[Groups('post')]
            private string $surname;

            public function getId(): int
            {
                return $this->id;
            }

            public function setId(int $id): self
            {
                $this->id = $id;

                return $this;
            }

            public function getName(): string
            {
                return $this->name;
            }

            public function setName(string $name): self
            {
                $this->name = $name;

                return $this;
            }

            public function getSurname(): string
            {
                return $this->surname;
            }

            public function setSurname(string $surname): self
            {
                $this->surname = $surname;

                return $this;
            }
        };
    }
}
