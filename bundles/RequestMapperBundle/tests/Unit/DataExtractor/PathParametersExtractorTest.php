<?php

namespace RequestMapperBundle\Tests\Unit\DataExtractor;

use PHPUnit\Framework\TestCase;
use RequestMapperBundle\DataExtractor\PathParametersExtractor;
use Symfony\Component\HttpFoundation\Request;

class PathParametersExtractorTest extends TestCase
{
    public function testGetGroups(): void
    {
        $extractor = new PathParametersExtractor();

        $this->assertContains('path', $extractor->getGroups());
    }

    public function testExtract(): void
    {
        $id = 100;
        $action = 'info';

        $extractor = new PathParametersExtractor();
        $request = new Request(['id' => $id], [], ['_route_params' => ['action' => $action]]);

        $result = $extractor->extract($request);

        $this->assertArrayNotHasKey('id', $result);
        $this->assertArrayHasKey('action', $result);
        $this->assertEquals($action, $result['action']);
    }
}
