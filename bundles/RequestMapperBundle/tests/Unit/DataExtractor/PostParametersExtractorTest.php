<?php

namespace RequestMapperBundle\Tests\Unit\DataExtractor;

use PHPUnit\Framework\TestCase;
use RequestMapperBundle\DataExtractor\PostParametersExtractor;
use Symfony\Component\HttpFoundation\Request;

class PostParametersExtractorTest extends TestCase
{
    public function testGetGroups(): void
    {
        $extractor = new PostParametersExtractor();

        $this->assertContains('post', $extractor->getGroups());
    }

    public function testExtract(): void
    {
        $id = 100;
        $name = 'John';
        $surname = 'Doe';

        $extractor = new PostParametersExtractor();
        $request = new Request(['id' => $id], ['name' => $name, 'surname' => $surname]);

        $result = $extractor->extract($request);

        $this->assertArrayNotHasKey('id', $result);
        $this->assertArrayHasKey('name', $result);
        $this->assertArrayHasKey('surname', $result);
        $this->assertEquals($name, $result['name']);
        $this->assertEquals($surname, $result['surname']);
    }
}
