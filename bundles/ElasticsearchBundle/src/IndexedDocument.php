<?php

namespace ElasticsearchBundle;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
class IndexedDocument
{
    protected string $indexName;

    public function __construct(string $indexName)
    {
        $this->indexName = $indexName;
    }

    public function getIndexName(): string
    {
        return $this->indexName;
    }
}
