<?php

namespace ElasticsearchBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ElasticsearchBundle extends Bundle
{
    public function getPath(): string
    {
        return dirname(__DIR__);
    }
}
