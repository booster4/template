<?php

namespace ElasticsearchBundle\DependencyInjection;

use ElasticsearchBundle\Connection;
use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class ElasticsearchExtension extends Extension
{
    /**
     * @throws Exception
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $location = new FileLocator(__DIR__ . '/../../config');
        $loader = new YamlFileLoader($container, $location);

        $loader->load('services.yaml');

        $this->configureConnectionServiceDefinition($config, $container);
    }

    /**
     * Set \ElasticsearchBundle\Connection::__constructor arguments
     *
     * @param array            $config
     * @param ContainerBuilder $container
     *
     * @return void
     */
    private function configureConnectionServiceDefinition(array $config, ContainerBuilder $container): void
    {
        $definition = $container->getDefinition(Connection::class);
        $definition->setArgument('$dsn', $config['connection']['dsn']);
    }
}
