<?php

namespace ElasticsearchBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('elasticsearch');

        $treeBuilder->getRootNode()
            ->children()
                ->arrayNode('connection')
                    ->isRequired()
                    ->children()
                        ->scalarNode('dsn')
                            ->isRequired()
                            ->cannotBeEmpty()
                            ->info('DSN of elasticsearch node')
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
