<?php

namespace ElasticsearchBundle;

use Elastic\Elasticsearch\Client;
use Elastic\Elasticsearch\ClientBuilder;
use Elastic\Elasticsearch\Exception\AuthenticationException;
use Elastic\Elasticsearch\Exception\ClientResponseException;
use Elastic\Elasticsearch\Exception\ServerResponseException;
use Elastic\Elasticsearch\Response\Elasticsearch;

class Connection
{
    private readonly Client $client;

    /**
     * @throws AuthenticationException
     */
    public function __construct(string $dsn)
    {
        $this->client = ClientBuilder::create()->setHosts([$dsn])->build();
    }

    /**
     * @throws ClientResponseException
     * @throws ServerResponseException
     */
    public function info(): Elasticsearch
    {
        return $this->client->info();
    }

    public function getClient(): Client
    {
        return $this->client;
    }
}
