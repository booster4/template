<?php

namespace ElasticsearchBundle;

use Elastic\Elasticsearch\Client;

interface ConnectionInterface
{
    public function getClient(): Client;
}
