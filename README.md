# Template

Install docker
```shell
brew install docker
brew install docker-compose
```

Build local images first:
```shell
make build
```

Start application local:
```shell
make up
```

Make migrations:
```shell
make migrate
```

Database diff:
```shell
make migration-diff
```
